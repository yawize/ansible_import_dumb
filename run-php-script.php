<?php
try
{
	$bdd = new PDO('mysql:host=localhost;dbname={{database_name}};charset=utf8', '{{database_login}}', '{{database_password}}');
}
catch(Exception $e)
{
        die('Erreur : '.$e->getMessage());
}


// Create Organisations
$bdd->exec("INSERT INTO `organizations` (`id`, `name`,
`survey_visio_1`, `survey_visio_3`, `survey_visio_4`,
`survey_meeting_1`, `survey_meeting_3`, `survey_meeting_4`,
`disclaimer`, `disclaimer_lang_2`, `disclaimer_lang_3`, `disclaimer_lang_4`,
`document_url`, `document_url_lang_2`, `document_url_lang_3`, `document_url_lang_4`,
`limit_resident_visit`, `limit_resident_video`, `limit_visitor_visit`, `limit_visitor_video`,
`limit_week_resident_visit`, `limit_week_resident_video`, `limit_week_visitor_visit`, `limit_week_visitor_video`) VALUES
(1, 'LIMIT_BY_DAYS',
NULL, NULL, NULL,
NULL, NULL, NULL,
NULL, NULL, NULL, NULL,
NULL, NULL, NULL, NULL,
1, 0, 0, 0,
0, 0, 0, 0),
(2, 'LIMIT_BY_WEEK',
NULL, NULL, NULL,
NULL, NULL, NULL,
NULL, NULL, NULL, NULL,
NULL, NULL, NULL, NULL,
0, 0, 0, 0,
1, 0, 0, 0)");

// Insert sites
$sites = "{{sites}}";

$sites = str_replace('\'', '"', $sites);

$sites = json_decode($sites, true);

foreach ($sites as $value) {

    $number = intval($value["u_numero"]);
    $name = $value["u_nom_site"];
    $category_type = "typz";
    $category_country = "SN";
    $zipcode = intval($value["code_postal"]);
    $city = $value["u_ville"];
    $email = $value["u_email_site"];
    $tel = $value["u_phone_site"];

    $bdd->exec("INSERT INTO `sites` (`id`, `organization_id`, `number`, `category_type`, `category_country`, `cgu`, `name`, `zipcode`, `city`, `tel`, `email`, `visio`, `meeting`, `visio_appointlet_url`,
    `meeting_appointlet_url`, `available`, `visible`) VALUES
  (1,  1, $number, '$category_type', '$category_country', '', '$name', $zipcode, '$city', '$tel', '$email', 1, 1, '', '', 1, 1)");
}

?>
